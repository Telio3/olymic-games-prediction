# Cas pratique 1 - Jeux Olympiques de Paris 2024

## 1. Dataset sélectionné

Nous avons utilisé le dataset [120 years of Olympic history: athletes and results](https://www.kaggle.com/datasets/heesoo37/120-years-of-olympic-history-athletes-and-results) provenant de Kaggle.

## 2. Notebook

Vous retrouverez notre notebook sur l'analyse de ce jeu de données [ici](./olympic_games.ipynb).\
*Toutes les réponses aux questions sont dans le notebook.*

## 3. Database

Nous avons opté pour une infrastructure PostgreSQL avec Docker, suivie de l'intégration de la bibliothèque Tortoise pour faciliter l'extraction des données de cette base.

### MCD

![MCD](./resources/DB_schema.png)

Nous avons alimenté la base données grâce aux fonctionnalités développées dans ce [fichier](./src/orm/data_fill.py).

## 4. Prédiction

Vous pouvez retrouver pour chaque question, son fichier d'entrainement et de prédiction ci-dessous :

- [Pouvez-vous prédire les pays médaillés dans au moins 2 disciplines lors des JO de Paris ?](./src/prediction/medal_country.py)
- [Pouvez-vous prédire les athlètes médaillés dans au moins 2 disciplines lors des JO de Paris ?](./src/prediction/athlete_country.py)
- [Pouvez-vous prédire le nombre total de médailles françaises lors des JO de Paris ?](./src/prediction/medal_france.py)

## 5. Article sur la distribution de médailles pendant le Covid

Cet article présente une approche méthodologique solide pour prédire les performances olympiques à l'aide d'un algorithme Random Forest en deux étapes lors de la periode Covid-19. L'article met en évidence l'importance de la prédiction précise des médailles olympiques pour divers acteurs, notamment les gouvernements, les sociétés de paris sportifs et les sponsors des Jeux olympiques. L'article démontre, également, une amélioration significative par rapport aux prévisions naïves et aux modèles précédemment publiés, ce qui renforce la crédibilité de l'approche proposée.

L'analyse des variables pertinentes et la méthodologie de validation du modèle renforcent la fiabilité des résultats obtenus. De plus, la section sur le processus de prévision souligne l'importance de la rigueur dans la séparation des données pour éviter les biais et garantir la validité des résultats.

En conclusion, cet article offre une contribution importante à la littérature sur la prédiction des performances olympiques, en mettant en avant une approche innovante et efficace. Il pourrait être bénéfique d'approfondir certains aspects pour une meilleure compréhension, mais dans l'ensemble, l'article offre une analyse solide et convaincante.

## 6. Conclusion sur notre étude 

Nous ne pourrions pas parier les yeux fermés sur un athlète ou même une nation car les prédictions ne sont que des suppositions via des précédentes données et la précision des résultats, de ces prédictions, est loin d'être parfaite. On peut envisager des favoris mais le facteur "chance" rentre en compte pour ce genre de sujet tel que le sport. La forme des athlètes, les changements d'entrainement, etc... peuvent engendrer tellement de différences que cette étude ne peut être qu'hypothèse.

## 7. À-t-on besoin du Big Data ?

Notre projet ne nécessite pas la mise en œuvre de solutions liées à la big data, car il ne rencontre pas les critères des "3V" caractéristiques de ce domaine : Volume, Variété et Vélocité.

- Volume : La big data est généralement associée à des volumes massifs de données, souvent bien au-delà de ce que peut présenter un fichier CSV de 130 000 lignes. L'ensemble de données est de taille modérée et peut être géré efficacement avec des ressources conventionnelles.

- Variété : La big data traite souvent des données provenant de diverses sources, comme des fichiers texte, des images, des vidéos, etc. Dans notre cas, notre unique fichier CSV suggère une homogénéité des données, ce qui réduit la complexité et la variété des informations à traiter.

- Vélocité : La big data est conçue pour traiter des flux de données en temps réel ou des mises à jour fréquentes. Notre projet, se basant sur un fichier statique, ne requiert pas une gestion de données en temps réel ou une vélocité de traitement rapide.

Ainsi, étant donné la taille modérée, la simplicité de structure et l'absence de besoins en temps réel dans notre projet, l'implémentation de solutions de big data serait disproportionnée par rapport aux exigences réelles. Des outils conventionnels sont suffisants pour gérer efficacement ce type de données.


<div style="text-align: right; margin-top: 100px;">
    CORRE Télio
    </br>
    DORÉ Mathis
</div>
