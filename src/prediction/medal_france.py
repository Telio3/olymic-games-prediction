import pandas as pd
from tortoise import run_async
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay, accuracy_score

from src.orm.models.participation import Participation
from src.orm.orm import init_database_connection, close_database_connection


async def main():
    # Récupérer les participations à partir de la base de données
    participations = await Participation.all().values(
        'athlete__country__name',
        'discipline__name',
        'medal__name'
    )

    # DataFrame Pandas des participations
    df = pd.DataFrame(participations)

    # Encoder les données
    label_encoder_country = LabelEncoder()
    label_encoder_discipline = LabelEncoder()
    label_encoder_medal = LabelEncoder()

    df['country'] = label_encoder_country.fit_transform(df['athlete__country__name'])
    df['discipline'] = label_encoder_discipline.fit_transform(df['discipline__name'])
    df['medal'] = label_encoder_medal.fit_transform(df['medal__name'].notnull())

    # Diviser les données en ensembles d'entraînement et de test
    X_train, X_test, y_train, y_test = train_test_split(df[['country', 'discipline']],
                                                        df['medal'], test_size=0.2, random_state=42)

    # Entraîner le modèle
    trained_model = RandomForestClassifier()
    trained_model.fit(X_train, y_train)

    # Faire des prédictions sur l'ensemble de test
    y_pred = trained_model.predict(X_test)

    # Afficher la matrice de confusion
    conf = confusion_matrix(y_test, y_pred)
    disp = ConfusionMatrixDisplay(confusion_matrix=conf, display_labels=label_encoder_medal.classes_)
    disp.plot()
    plt.show()

    # Évaluer la précision du modèle
    accuracy = accuracy_score(y_test, y_pred)
    print(f'Accuracy: {accuracy}')

    # Disciplines pour lesquelles nous voulons prédire le nombre total de médailles pour la France
    disciplines = ["Swimming Women's 200 metres Backstroke", "Cycling Men's Team Pursuit, 4,000 metres",
                              "Art Competitions Mixed Literature", "Handball Men's Handball",
                              "Alpine Skiing Women's Slalom", "Gymnastics Men's Team All-Around"]

    # Créer un DataFrame pour stocker les prédictions
    predictions_df = pd.DataFrame(columns=['discipline', 'predicted_medals'])

    # Préparer les données pour la prédiction
    france_data = {'country': label_encoder_country.transform(['France'] * len(disciplines)),
                   'discipline': label_encoder_discipline.transform(disciplines)}

    # Faire les prédictions
    predictions_france = trained_model.predict(pd.DataFrame(france_data))

    # Remplir le DataFrame avec les résultats
    predictions_df['discipline'] = disciplines
    predictions_df['predicted_medals'] = predictions_france

    # Afficher les résultats
    print(predictions_df)

    # Nombre total de médailles prédites
    print(f'Total number of medals predicted: {predictions_france.sum()}')


# Initialisation de la connexion à la base de données
run_async(init_database_connection())

# Exécution de la fonction principale
run_async(main())

# Fermeture de la connexion à la base de données
run_async(close_database_connection())
