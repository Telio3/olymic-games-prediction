import pandas as pd
from tortoise import run_async
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay, accuracy_score

from src.orm.models.participation import Participation
from src.orm.orm import init_database_connection, close_database_connection


async def main():
    # Récupérer les participations à partir de la base de données
    participations = await Participation.all().values(
        'athlete__country__name',
        'discipline__name',
        'medal__name'
    )

    # DataFrame Pandas des participations
    df = pd.DataFrame(participations)

    # Encoder les données
    label_encoder_country = LabelEncoder()
    label_encoder_discipline = LabelEncoder()
    label_encoder_medal = LabelEncoder()

    df['country'] = label_encoder_country.fit_transform(df['athlete__country__name'])
    df['discipline'] = label_encoder_discipline.fit_transform(df['discipline__name'])
    df['medal'] = label_encoder_medal.fit_transform(df['medal__name'].notnull())

    # Diviser les données en ensembles d'entraînement et de test
    X_train, X_test, y_train, y_test = train_test_split(df[['country', 'discipline']],
                                                        df['medal'], test_size=0.2, random_state=42)

    # Entraîner le modèle
    trained_model = RandomForestClassifier()
    trained_model.fit(X_train, y_train)

    # Faire des prédictions sur l'ensemble de test
    y_pred = trained_model.predict(X_test)

    # Afficher la matrice de confusion
    conf = confusion_matrix(y_test, y_pred)
    disp = ConfusionMatrixDisplay(confusion_matrix=conf, display_labels=label_encoder_medal.classes_)
    disp.plot()
    plt.show()

    # Évaluer la précision du modèle
    accuracy = accuracy_score(y_test, y_pred)
    print(f'Accuracy: {accuracy}')

    # Utiliser le modèle entraîné pour faire des prédictions sur de nouvelles données
    discipline = "Football Men's Football"
    countries_to_predict = ["France", "United States", "Germany", "China", "Spain", "Ghana", "Brazil", "Italy",
                            "Japan", "Australia", "Argentina", "Poland"]

    new_data = {'country': label_encoder_country.transform(countries_to_predict),
                'discipline': label_encoder_discipline.transform([discipline] * len(countries_to_predict))}

    # Faire la prédiction
    predictions_new_data = trained_model.predict(pd.DataFrame(new_data))

    # Filtrer les pays ayant au moins une médaille
    countries_with_medals = [countries_to_predict[i] for i in range(len(countries_to_predict)) if
                             predictions_new_data[i] >= 1]

    print(f'Countries predicted to have at least one medal in {discipline}: {countries_with_medals}')


# Initialisation de la connexion à la base de données
run_async(init_database_connection())

# Exécution de la fonction principale
run_async(main())

# Fermeture de la connexion à la base de données
run_async(close_database_connection())
