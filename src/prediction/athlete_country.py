import pandas as pd
from tortoise import run_async
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay, accuracy_score

from src.orm.models.participation import Participation
from src.orm.orm import init_database_connection, close_database_connection


async def main():
    # Récupérer les participations à partir de la base de données
    participations = await Participation.all().values(
        'athlete__full_name',
        'discipline__name',
        'medal__name'
    )

    # DataFrame Pandas des participations
    df = pd.DataFrame(participations)

    # Encoder les données
    label_encoder_athlete = LabelEncoder()
    label_encoder_discipline = LabelEncoder()
    label_encoder_medal = LabelEncoder()

    df['athlete'] = label_encoder_athlete.fit_transform(df['athlete__full_name'])
    df['discipline'] = label_encoder_discipline.fit_transform(df['discipline__name'])
    df['medal'] = label_encoder_medal.fit_transform(df['medal__name'].notnull())

    # Diviser les données en ensembles d'entraînement et de test
    X_train, X_test, y_train, y_test = train_test_split(df[['athlete', 'discipline']],
                                                        df['medal'], test_size=0.2, random_state=42)

    # Initialiser le classificateur RandomForest
    classifier = RandomForestClassifier(n_estimators=100, random_state=42)

    # Entraîner le modèle sur l'ensemble d'entraînement
    classifier.fit(X_train, y_train)

    # Prédire les classes sur l'ensemble de test
    y_pred = classifier.predict(X_test)

    # Afficher la matrice de confusion
    conf = confusion_matrix(y_test, y_pred)
    disp = ConfusionMatrixDisplay(confusion_matrix=conf, display_labels=label_encoder_medal.classes_)
    disp.plot()
    plt.show()

    # Évaluer la précision du modèle
    accuracy = accuracy_score(y_test, y_pred)
    print(f'Accuracy: {accuracy}')

    # Athlètes pour lesquels nous voulons prédire s'ils vont gagner une médaille
    athletes_to_predict = ['Arvo Ossian Aaltonen', 'Emilio Julio Abreu', 'Jikirum Adjaluddin']

    new_data = {'athlete': athletes_to_predict,
                'discipline': ["Swimming Men's 200 metres Breaststroke"] * len(athletes_to_predict)}
    new_df = pd.DataFrame(new_data)

    # Encoder les nouvelles données
    new_df['athlete'] = label_encoder_athlete.transform(new_df['athlete'])
    new_df['discipline'] = label_encoder_discipline.transform(new_df['discipline'])

    # Faire des prédictions
    predictions = classifier.predict(new_df[['athlete', 'discipline']])

    # Décoder les prédictions
    predicted_medals = label_encoder_medal.inverse_transform(predictions)

    print("Predicted Medals:", predicted_medals)


# Initialisation de la connexion à la base de données
run_async(init_database_connection())

# Exécution de la fonction principale
run_async(main())

# Fermeture de la connexion à la base de données
run_async(close_database_connection())
