import math

import pandas as pd
from tortoise import run_async

from src.orm.models.athlete import Athlete
from src.orm.models.country import Country
from src.orm.models.game import Game
from src.orm.models.medal import Medal
from src.orm.models.participation import Participation
from src.orm.orm import init_database_connection, close_database_connection
from src.orm.models.discipline import Discipline

# Initialisation de la connexion à la base de données
run_async(init_database_connection())


# Fonction pour réinitialiser la base de données en supprimant tous les enregistrements de différentes tables
async def reset():
    await Participation.all().delete()
    await Athlete.all().delete()
    await Country.all().delete()
    await Medal.all().delete()
    await Game.all().delete()
    await Discipline.all().delete()

# Exécution de la fonction de réinitialisation de la base de données
run_async(reset())

# Lecture d'un fichier CSV dans un DataFrame Pandas
df = pd.read_csv('../../datasets/athlete_events.csv')

# Extraction des valeurs uniques de colonnes spécifiques dans le DataFrame
unique_disciplines = df['Event'].unique()
unique_games = df[['Games', 'Year']].drop_duplicates()
df_without_na = df.dropna(subset=['Medal'])
unique_medals = df_without_na['Medal'].unique()
unique_countries = df['Team'].unique()
unique_athletes = df[['Name', 'Team']].drop_duplicates()


# Fonction pour convertir une valeur en entier, en gérant les valeurs NaN
def convert_to_int(value):
    try:
        if math.isnan(value):
            return 0
        else:
            return int(value)
    except TypeError:
        pass


# Fonction pour remplir la base de données avec des données
async def data_fill():
    # Insertion des disciplines uniques dans la table Discipline
    for discipline in unique_disciplines:
        await Discipline.create(name=discipline)

    # Insertion des jeux uniques dans la table Game
    for index, row in unique_games.iterrows():
        name = row['Games']
        year = convert_to_int(row['Year'])
        await Game.create(name=name, year=year)

    # Insertion des médailles uniques dans la table Medal
    for medal in unique_medals:
        await Medal.create(name=medal)

    # Insertion des pays uniques dans la table Country
    for country in unique_countries:
        await Country.create(name=country)

    # Insertion des athlètes uniques dans la table Athlete, en les associant à leurs pays respectifs
    for index, row in unique_athletes.iterrows():
        full_name = row['Name']
        country = await Country.get(name=row['Team'])
        await Athlete.create(full_name=full_name, country=country)

    # Insertion des enregistrements de participation dans la table Participation
    for index, row in df.iterrows():
        discipline = await Discipline.get(name=row['Event'])
        game = await Game.get(name=row['Games'], year=convert_to_int(row['Year']))
        medal = await Medal.get_or_none(name=row['Medal'])
        athlete = await Athlete.get(full_name=row['Name'], country__name=row['Team'])
        await Participation.create(discipline=discipline, game=game, medal=medal, athlete=athlete)

# Exécution de la fonction de remplissage des données
run_async(data_fill())

# Fermeture de la connexion à la base de données
run_async(close_database_connection())
