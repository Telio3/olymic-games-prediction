from tortoise.models import Model
from tortoise import fields


class Medal(Model):
    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=10)

    def __str__(self):
        return self.name
