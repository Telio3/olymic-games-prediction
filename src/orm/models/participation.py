from tortoise.models import Model
from tortoise import fields


class Participation(Model):
    id = fields.IntField(pk=True)
    game = fields.ForeignKeyField('models.Game', related_name='participations')
    discipline = fields.ForeignKeyField('models.Discipline', related_name='participations')
    medal = fields.ForeignKeyField('models.Medal', related_name='participations', null=True)
    athlete = fields.ForeignKeyField('models.Athlete', related_name='participations')

    def __str__(self):
        return f'{self.athlete} - {self.game} - {self.discipline} - {self.medal}'
