from tortoise.models import Model
from tortoise import fields


class Game(Model):
    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=255)
    year = fields.IntField()

    def __str__(self):
        return self.name
