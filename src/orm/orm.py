from tortoise import Tortoise


async def init_database_connection():
    await Tortoise.init(
        db_url='postgres://myuser:mypass@127.0.0.1:5432/mydb',
        modules={'models': ['src.orm.models.athlete', 'src.orm.models.country', 'src.orm.models.discipline',
                            'src.orm.models.game', 'src.orm.models.medal', 'src.orm.models.participation']}
    )

    await Tortoise.generate_schemas()


async def close_database_connection():
    await Tortoise.close_connections()
